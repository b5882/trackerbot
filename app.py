from telegram.ext import (
    CommandHandler,
    MessageHandler,
    ConversationHandler,
    Filters
)

from config import dispatcher, updater
import handlers.base as basehandlers
import handlers.owneractions as ownerhanders
import scheduler

if __name__ == '__main__':
    scheduler.start_scheduler()

    dispatcher.add_handler(CommandHandler('start', basehandlers.start))
    dispatcher.add_handler(CommandHandler('help', basehandlers.get_help))
    dispatcher.add_handler(MessageHandler(Filters.regex(r'^(Список)$'), ownerhanders.gadget_list))

    set_owner_handler = ConversationHandler(
        entry_points=[MessageHandler(Filters.regex(r'^(Занять)$'), ownerhanders.try_set_owner)],
        states={
            ownerhanders.ENTER_IDS: [
                MessageHandler(Filters.regex(r'^(Отмена)$'), basehandlers.cancel),
                MessageHandler(Filters.text, ownerhanders.set_owner_enter_ids)
            ],
            ownerhanders.NEED_CLARIFY: [
                MessageHandler(Filters.regex(r'^(Отмена)$'), basehandlers.cancel),
                MessageHandler(Filters.text, ownerhanders.set_owner_clarify),
            ]
        },
        fallbacks=[CommandHandler("cancel", basehandlers.cancel),
                   MessageHandler(Filters.regex(r'^(Отмена)$'), basehandlers.cancel)]
    )
    dispatcher.add_handler(set_owner_handler)

    drop_owner_handler = ConversationHandler(
        entry_points=[MessageHandler(Filters.regex(r'^(Освободить)$'), ownerhanders.try_drop_owner)],
        states={
            ownerhanders.ENTER_IDS: [
                MessageHandler(Filters.regex(r'^(Отмена)$'), basehandlers.cancel),
                MessageHandler(Filters.text, ownerhanders.drop_owner_enter_ids)
            ]
        },
        fallbacks=[CommandHandler("cancel", basehandlers.cancel),
                   MessageHandler(Filters.regex(r'^(Отмена)$'), basehandlers.cancel)]
    )
    dispatcher.add_handler(drop_owner_handler)

    registration_handler = ConversationHandler(
        entry_points=[CommandHandler('registration', basehandlers.registration_start)],
        states={
            basehandlers.WAIT_ENTER: [
                MessageHandler(Filters.text, basehandlers.registration_end)
            ]
        },
        fallbacks=[CommandHandler("cancel", basehandlers.cancel),
                   MessageHandler(Filters.regex(r'^(Отмена)$'), basehandlers.cancel)]
    )
    dispatcher.add_handler(registration_handler)

    subscription_handler = ConversationHandler(
        entry_points=[MessageHandler(Filters.regex(r'^(Подписаться)$'), ownerhanders.subs_handler.subscribe_start)],
        states={
            ownerhanders.SUBSCRIBE_ENTER_IDS: [
                MessageHandler(Filters.regex(r'^(Отмена)$'), basehandlers.cancel),
                MessageHandler(Filters.text, ownerhanders.subs_handler.subscribe_enter_ids)
            ]
        },
        fallbacks=[CommandHandler("cancel", basehandlers.cancel),
                   MessageHandler(Filters.regex(r'^(Отмена)$'), basehandlers.cancel)]
    )
    dispatcher.add_handler(subscription_handler)

    updater.start_polling()  # Переделать на вэбхук



import os
import secrets
from sqlalchemy import create_engine
from sqlalchemy.orm import Session
import db.dbmodel as dbmodel

# почему-то при w+ режиме содержимое файла не читается
token_path = os.getcwd() + '/data/registertoken.txt'
access_mode = 'r' if os.path.exists(token_path) else 'w'
with open(token_path, access_mode) as file:
    if access_mode == 'w':
        register_token = str(secrets.token_hex(16))
        file.write(register_token)
    else:
        register_token = file.readline().strip()

new_db = False
db_path = os.getcwd() + '/data/users.db'

if not os.path.exists(db_path):
    open(db_path, 'w+').close()
    new_db = True

db_engine = create_engine('sqlite:///' + db_path)
dbmodel.SqlOrmBase.metadata.create_all(db_engine)
session = Session(bind=db_engine)

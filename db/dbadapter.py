from db.dbconfig import session, register_token
from db.dbmodel import User, Message


def registration(token, tg_id, name) -> bool:
    """Store user to the registered user table if given token is valid"""
    if token == register_token:
        user = User(tg_id=tg_id, full_name=name)
        session.add(user)
        session.commit()

    return token == register_token


def check_registration(tg_id) -> bool:
    """Check contains user into registered user table"""
    return True if session.query(User).filter(User.tg_id == tg_id).first() else False


def save_msg(tg_id, msg, state) -> None:
    """Store given message and state to the users' message table"""
    row = session.query(Message).filter(Message.tg_id == tg_id, Message.state == state).first()
    if row:
        row.message = msg
    else:
        row = Message(tg_id=tg_id, message=msg, state=state)
    session.add(row)
    session.commit()


def get_msg(tg_id, state) -> str:
    """Extract message from the users' message table by user tg id and state.
    Return empty string if user or state not found into db"""
    msg = session.query(Message.message).filter(Message.tg_id == tg_id, Message.state == state).first()
    return msg[0] if msg else ""


def remove_msg(tg_id, state=None) -> None:
    """Remove row from the users' message table by tg_id and state.
    If state==None then remove all rows by tg_id"""
    if state:
        rows = session.query(Message).filter(Message.tg_id == tg_id, Message.state == state).all()
    else:
        rows = session.query(Message).filter(Message.tg_id == tg_id).all()

    for row in rows:
        session.delete(row)

    session.commit()

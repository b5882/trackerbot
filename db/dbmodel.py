from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
from datetime import datetime

SqlOrmBase = declarative_base()


class User(SqlOrmBase):
    """Table for storing registered users"""
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    tg_id = Column(Integer, nullable=False, unique=True)
    full_name = Column(String(100))
    registred = Column(DateTime(), default=datetime.now)


class Message(SqlOrmBase):
    """Table for storing users' messages between state"""
    __tablename__ = 'messages'
    id = Column(Integer, primary_key=True)
    tg_id = Column(Integer, nullable=False)
    state = Column(Integer)
    message = Column(String)

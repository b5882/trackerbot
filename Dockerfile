FROM alpine:latest as builder

WORKDIR /app

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apk update && apk add --no-cache python3 py3-pip 
RUN apk add --no-cache build-base

COPY requirements.txt .

RUN pip wheel --no-cache-dir --no-deps --wheel-dir /app/wheels -r requirements.txt


FROM alpine:latest

WORKDIR /app

RUN apk update && apk add --no-cache python3 py3-pip tzdata

COPY --from=builder /app/wheels /wheels
COPY . .

RUN pip install --no-cache /wheels/*

ENV TZ=Europe/Moscow

ENTRYPOINT ["python3", "./app.py"]
CMD ["127.0.0.1:5000"]

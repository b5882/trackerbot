import time
import schedule as shd
import threading
import config
from restclient.client import RestClient

api = RestClient(config.server_url)

shd.every().day.at('01:00').do(api.clear_all_owners)


def start_scheduler():
    job_thread = threading.Thread(target=loop)
    job_thread.start()


def loop():
    while True:
        shd.run_pending()
        time.sleep(30)

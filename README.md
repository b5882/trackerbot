Create docker image:

1. Save your access token to token.txt (for example ~/bot/data/token.txt)
2. ```docker build -t bposbot/alpine```
3. ```docker run -d -v ~/bot/data:/app/data --restart always --network host bposbot/alpine```

By default bot search rest server by 127.0.0.1:5000 address. If you want to change it you can change 3 command to:
```docker run -d -v ~/bot/data:/app/data --restart always --network host bposbot/alpine 'youraddress'```
Full url will form 'http://youraddress/api'.

I recomend use docker compose to deploy bot + server containers.
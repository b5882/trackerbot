from telegram.ext import Updater
from sys import argv
import logging
import os

server_url = 'http://127.0.0.1:5000/api' if len(argv) < 2 else 'http://' + argv[1] + '/api'

logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

token_path = os.getcwd() + '/data/token.txt'
with open(token_path, 'r') as file:
    updater = Updater(file.readline().strip(), use_context=True)

dispatcher = updater.dispatcher
bot = updater.bot
from telegram import Update
from telegram.ext import CallbackContext, ConversationHandler
from restclient.client import RestClient, ApiKeys
from handlers.base import check_registration
import markup.keybord as mrk_kbd
import markup.messages as mrk_msg
from handlers.subscription import SubscriptionHandler, SUBSCRIBE_ENTER_IDS, SUBSCRIBE_START
import re
import config
import db.dbadapter as db

START, ENTER_IDS, NEED_CLARIFY = range(3)

rest_client = RestClient(config.server_url)
subs_handler = SubscriptionHandler(rest_client, config.bot)


@check_registration
def gadget_list(update: Update, context: CallbackContext) -> None:
    responce = 'Список устройств:\n'
    status, gadgets = rest_client.get_gadgets()
    if status == RestClient.SUCCESS:
        for gadget in gadgets:
            responce += (str(gadget[ApiKeys.id]) + '. ' +
                         gadget[ApiKeys.name] + ' - ' +
                         gadget[ApiKeys.config] + ' - ' +
                         gadget[ApiKeys.comment] + ' - ' +
                         gadget[ApiKeys.user] + ' - ' +
                         str(gadget[ApiKeys.timestamp])) + '\n'

        update.message.reply_text(responce, reply_markup=mrk_kbd.main_markup)
    elif status == RestClient.CONNECTION_ERROR:
        update.message.reply_text(mrk_msg.error_server_connection_msg, reply_markup=mrk_kbd.main_markup)
    else:
        update.message.reply_text(mrk_msg.error_unknown_msg, reply_markup=mrk_kbd.main_markup)


@check_registration
def try_set_owner(update: Update, context: CallbackContext) -> int:
    update.message.reply_text(mrk_msg.set_owner_enter_ids_msg, reply_markup=mrk_kbd.cancel_only_markup)
    return ENTER_IDS


@check_registration
def set_owner_enter_ids(update: Update, context: CallbackContext) -> int:
    message = update.message.text
    if re.match(r'^((\d?\,?\s?))+\d+?$', message):
        ids = {int(i) for i in message.split(',')}
        status, gadgets = rest_client.get_gadgets()

        if status == RestClient.SUCCESS:
            busy_ids = {int(gadget[ApiKeys.id]) for gadget in gadgets
                        if gadget[ApiKeys.user] != '-' and gadget[ApiKeys.user] != update.message.from_user.name}
            intersection = busy_ids & ids
            if intersection:
                db.save_msg(update.message.from_user.id, message, ENTER_IDS)
                update.message.reply_text(mrk_msg.set_owner_clarify_choice_msg.format(str(intersection)),
                                          reply_markup=mrk_kbd.choice_markup)
                return NEED_CLARIFY
            else:
                __set_owner(update, (int(id) for id in ids))

        elif status == RestClient.CONNECTION_ERROR:
            update.message.reply_text(mrk_msg.error_server_connection_msg)
            return ConversationHandler.END
    else:
        update.message.reply_text(mrk_msg.common_invalid_enter_msg)
        return ENTER_IDS

    gadget_list(update, context)
    return ConversationHandler.END


@check_registration
def set_owner_clarify(update: Update, context: CallbackContext) -> int:
    message = update.message.text
    if message.lower() == 'да':
        prev_msg = db.get_msg(update.message.from_user.id, ENTER_IDS)
        ids = prev_msg.split(',')
        __set_owner(update, (int(id) for id in ids))
    elif message.lower() == 'нет':
        update.message.reply_text(mrk_msg.set_owner_cancel_msg, reply_markup=mrk_kbd.main_markup)
    else:
        update.message.reply_text(mrk_msg.error_invalid_enter_msg + '\n' + mrk_msg.common_repeat_enter_msg)
        return NEED_CLARIFY

    db.remove_msg(update.message.from_user.id, ENTER_IDS)
    gadget_list(update, context)
    return ConversationHandler.END


def __set_owner(update: Update, ids) -> None:
    success_take = []
    error_take = []
    for id in ids:
        status = rest_client.set_gadget_owner(int(id), update.message.from_user.name)
        if status == RestClient.CONNECTION_ERROR:
            update.message.reply_text(mrk_msg.error_server_connection_msg)
        elif status == RestClient.SUCCESS:
            success_take.append(id)
        else:
            error_take.append(id)

    if error_take:
        if success_take:
            update.message.reply_text(mrk_msg.set_owner_success_msg.format(success_take))
        update.message.reply_text(mrk_msg.set_owner_error_msg.format(error_take))


@check_registration
def try_drop_owner(update: Update, context: CallbackContext) -> int:
    update.message.reply_text(mrk_msg.drop_owner_enter_ids_msg, reply_markup=mrk_kbd.drop_owner_markup)
    return ENTER_IDS


@check_registration
def drop_owner_enter_ids(update: Update, context: CallbackContext) -> int:
    message = update.message.text
    status, gadgets = rest_client.get_gadgets()

    if status == RestClient.SUCCESS:
        if message.lower() == 'all' or message.lower() == 'освободить все':
            ids = (int(gadget[ApiKeys.id]) for gadget in gadgets
                   if gadget[ApiKeys.user] == update.message.from_user.name)
        elif re.match(r'^((\d?\,?\s?))+\d+?$', message):
            ids = {int(dev_id) for dev_id in message.split(',')}
        else:
            update.message.reply_text(mrk_msg.common_invalid_enter_msg)
            return ENTER_IDS

        gadget_owners = {gadget[ApiKeys.id]: gadget[ApiKeys.user] for gadget in gadgets}
        for id in ids:
            if id not in gadget_owners:
                update.message.reply_text(mrk_msg.error_invalid_id_msg.format(str(id)))
            elif gadget_owners[id] == update.message.from_user.name:
                status = rest_client.drop_gadget_owner(id)
                if status == RestClient.CONNECTION_ERROR:
                    update.message.reply_text(mrk_msg.error_server_connection_msg, reply_markup=mrk_kbd.main_markup)
                    return ConversationHandler.END
                elif status != RestClient.SUCCESS:
                    update.message.reply_text(mrk_msg.common_invalid_enter_msg, reply_markup=mrk_kbd.main_markup)
                    return ENTER_IDS
                else:
                    subs_handler.notify_all(id)
            else:
                update.message.reply_text(mrk_msg.error_drop_owner_msg + str(id), reply_markup=mrk_kbd.main_markup)

    elif status == RestClient.CONNECTION_ERROR:
        update.message.reply_text(mrk_msg.error_server_connection_msg, reply_markup=mrk_kbd.main_markup)
        return ConversationHandler.END

    gadget_list(update, context)
    return ConversationHandler.END

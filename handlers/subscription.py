from telegram import Update, TelegramError
from telegram.ext import CallbackContext, ConversationHandler
from restclient.client import RestClient, ApiKeys
from handlers.base import check_registration
import markup.keybord as mrk_kbd
import markup.messages as mrk_msg
import re

SUBSCRIBE_START, SUBSCRIBE_ENTER_IDS = range(3, 5)


class SubscriptionHandler:
    """ A class for processing subscriptions to devices."""
    def __init__(self, rest_client, bot):
        self.__bot = bot
        self.__subscriptions = {} # {dev_id: {usr_id: chat_id}}
        self.__rest_client = rest_client

    def notify_all(self, id: int) -> None:
        """ Notify all subscribers"""
        if id in self.__subscriptions:
            for usr_id in self.__subscriptions[id].values():
                try:
                    self.__bot.send_message(usr_id, mrk_msg.subscribe_notify_msg.format(id))
                except TelegramError:
                    pass # add logger
            del self.__subscriptions[id]
            

    #@check_registration
    def subscribe_start(self, update: Update, context: CallbackContext) -> int:
        """ Handler for ConversationHandler. Init state. """
        update.message.reply_text(mrk_msg.subscribe_device_msg, reply_markup=mrk_kbd.cancel_only_markup)
        return SUBSCRIBE_ENTER_IDS

    #@check_registration
    def subscribe_enter_ids(self, update: Update, context: CallbackContext) -> int:
        """ Handler for ConversationHandler. Waite input user message. """
        message = update.message.text

        if not re.match(r'^((\d?\,?\s?))+\d+?$', message):
            update.message.reply_text(mrk_msg.common_invalid_enter_msg)
            return SUBSCRIBE_ENTER_IDS

        ids = {int(dev_id) for dev_id in message.split(',')}        
        self.__subscribe(ids, update.message.chat_id, update)
        return ConversationHandler.END

    def __subscribe(self, ids, chat_id: int, update: Update) -> None:
        """ Handles subscription to device """
        status, gadgets = self.__rest_client.get_gadgets()

        if status != RestClient.SUCCESS:
            update.message.reply_text(mrk_msg.error_server_connection_msg, reply_markup=mrk_kbd.main_markup)
            return

        devices = {gadget[ApiKeys.id]: gadget[ApiKeys.user] for gadget in gadgets}
        dev_is_free = []
        dev_to_sub = []
        dev_to_ignore = []
        for id in ids:
            if id in devices:
                if devices[id] == '-' or devices[id] == update.message.from_user.name:
                    dev_is_free.append(id)
                else:
                    dev_to_sub.append(id)
            else:
                dev_to_ignore.append(id)

        if dev_to_ignore:
            update.message.reply_text(mrk_msg.error_invalid_id_msg.format(dev_to_ignore), reply_markup=mrk_kbd.main_markup)
        if dev_is_free:
            update.message.reply_text(mrk_msg.subscribe_not_need_reply_msg.format(dev_is_free), reply_markup=mrk_kbd.main_markup)
        if dev_to_sub:
            for id in dev_to_sub:
                if id in self.__subscriptions and update.message.from_user.id not in self.__subscriptions[id]:
                    self.__subscriptions[id][update.message.from_user.id] = chat_id                        
                else:
                    self.__subscriptions[id] = {update.message.from_user.id: chat_id}

            update.message.reply_text(mrk_msg.subscribe_ok_reply_msg.format(dev_to_sub),
                                      reply_markup=mrk_kbd.main_markup)

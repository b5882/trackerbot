from telegram import Update
from telegram.ext import CallbackContext, ConversationHandler
import db.dbadapter as registrator
import markup.keybord as mrk_kbd
import markup.messages as mrk_msg


START, WAIT_ENTER = range(2)


def check_registration(func_to_decorate):
    def wrapper(update: Update, *args, **kargs):
        if registrator.check_registration(update.message.from_user.id):
            return func_to_decorate(update, *args, **kargs)
        else:
            update.message.reply_text(mrk_msg.registration_main_msg)
    return wrapper


def registration_start(update: Update, context: CallbackContext) -> int:
    update.message.reply_text(mrk_msg.registration_token_msg)
    return WAIT_ENTER


def registration_end(update: Update, context: CallbackContext) -> int:
    if registrator.registration(update.message.text,
                                update.message.from_user.id,
                                update.message.from_user.full_name):
        update.message.reply_text(mrk_msg.registration_success_msg)
        start(update, context)
        return ConversationHandler.END
    else:
        update.message.reply_text(mrk_msg.registration_error_msg)
        return WAIT_ENTER


def cancel(update: Update, context: CallbackContext) -> int:
    update.message.reply_text(mrk_msg.common_cancel_command_msg, reply_markup=mrk_kbd.main_markup)
    return ConversationHandler.END


@check_registration
def start(update: Update, context: CallbackContext) -> None:
    update.message.reply_text(mrk_msg.common_menu_msg, reply_markup=mrk_kbd.main_markup)


@check_registration
def get_help(update: Update, context: CallbackContext) -> None:
    update.message.reply_text(mrk_msg.common_help_msg)

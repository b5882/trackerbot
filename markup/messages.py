# Set owner msgs
set_owner_clarify_choice_msg = ('Некоторые id: {} пренадлежат другим пользователям!\n'
                                'Сменить владельца?')
set_owner_cancel_msg = 'Ресурсы не заняты'
set_owner_success_msg = 'Ресурсы id: {} успешно заняты'
set_owner_error_msg = 'Не удалось занять ресурсы id: {}'
set_owner_enter_ids_msg = 'Через запятую введите номера ресурсов которые хотите занять'

# Subscribe to device drop owner msg
subscribe_device_msg = ('Об освобождении каких девайсов вас уведомить?\n'
                        'Введите индексы через запятую.')
subscribe_ok_reply_msg = 'Ок, напишу вам как только девайсы: {} освободятся!'
subscribe_not_need_reply_msg = 'Девайсы: {} и так свободны, просто займите их!'
subscribe_notify_msg = 'Устройство id: {} освободилось!'

# Drop owner msgs
drop_owner_enter_ids_msg = ("Через запятую введите номера ресурсов которые хотите освободить.\n" 
                            "'all' для освобождения всех занятых ресурсов.")

# Error msgs
error_server_connection_msg = 'Похоже сервер ресурсов не доступен'
error_unknown_msg = 'Неизвестная ошибка'
error_drop_owner_msg = 'Невозможно освободить ресурсы, владельцем которого не являетесь id: '
error_invalid_id_msg = 'Не корректный id={}'
error_invalid_enter_msg = 'Некоректный ввод'

# Common msgs
common_repeat_enter_msg = 'Повторите запрос'
common_cancel_command_msg = 'Последняя команда отменена'
common_menu_msg = 'Меню'
common_help_msg = ("/start - для начала работы с ботом\n" 
                   "/cancel - для cброса бота в начало\n" 
                   "'Занять' - забрать ресурс\n" 
                   "'Освободить' - освободить ресурс\n" 
                   "'Список' - список доступных ресурсов\n" 
                   "'Редактирование' - добавление/удаление ресурсов")
common_invalid_enter_msg = ('Не корректный ввод.\n'
                            'Введите индексы через запятую.\n'
                            'Для выхода нажмите "Отмена".')


# Registration msgs
registration_main_msg = 'Для использования необходима регистрация. Выполните команду /registration'
registration_token_msg = 'Введите токен для регистрации'
registration_success_msg = 'Регистрация успешна!'
registration_error_msg = 'Ошибка регистрации! Попробуйте другой токен!'


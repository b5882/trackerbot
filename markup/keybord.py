from telegram import ReplyKeyboardMarkup

main_keyboard = [['Занять', 'Освободить'],
                 ['Подписаться', 'Список']]
main_markup = ReplyKeyboardMarkup(main_keyboard,
                                  resize_keyboard=True,
                                  input_field_placeholder='Choose action')


cancel_only_keyboard = [['Отмена']]
cancel_only_markup = ReplyKeyboardMarkup(cancel_only_keyboard,
                                         resize_keyboard=True,
                                         input_field_placeholder='Choose action')


drop_owner_keyboard = [['Освободить все', 'Отмена']]
drop_owner_markup = ReplyKeyboardMarkup(drop_owner_keyboard,
                                        resize_keyboard=True,
                                        input_field_placeholder='Choose action')


choice_keyboard = [['Да', 'Нет'], ['Отмена']]
choice_markup = ReplyKeyboardMarkup(choice_keyboard,
                                    resize_keyboard=True,
                                    input_field_placeholder='Choose action')


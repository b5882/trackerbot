import requests
import json


class ApiKeys:
    name = 'name'
    config = 'config'
    id = 'id'
    comment = 'comment'
    user = 'user'
    timestamp = 'timestamp'


class RestClient:
    """Class for interacting with the rest api.
    The server_reply variable stores the reason for the error or the contents of a successful request."""

    SUCCESS, SERVER_ERROR, CONNECTION_ERROR = range(3)

    def __init__(self, url):
        """Take base url address to connect"""
        self.url = url
        self.server_reply = ''
        self.last_status = 200

    def get_gadgets(self) -> tuple:
        """Return tuple(status code, gadget list). If status code != SUCCESS then gadget list is empty"""
        try:
            reply = requests.get(self.url + '/machines')
            self.server_reply = reply.text
            self.last_status = reply.status_code
            if reply.status_code == 200:
                return self.SUCCESS, json.loads(reply.text)
            else:
                return self.SERVER_ERROR, []
        except requests.exceptions.ConnectionError:
            self.server_reply = 'Server connection error'
            return self.CONNECTION_ERROR, []

    def set_gadget_owner(self, id: int, user: str) -> int:
        """Return status code"""
        try:
            reply = requests.put(self.url + '/machines/set/{0}'.format(id), json={'user': user})
            self.server_reply = reply.text
            self.last_status = reply.status_code
            return self.SUCCESS if reply.status_code == 200 else self.SERVER_ERROR
        except requests.exceptions.ConnectionError:
            self.server_reply = 'Server connection error'
            return self.CONNECTION_ERROR

    def drop_gadget_owner(self, id: int) -> int:
        """Return status code"""
        try:
            reply = requests.put(self.url + '/machines/drop/{0}'.format(id))
            self.server_reply = reply.text
            self.last_status = reply.status_code
            return self.SUCCESS if reply.status_code == 200 else self.SERVER_ERROR
        except requests.exceptions.ConnectionError:
            self.server_reply = 'Server connection error'
            return self.CONNECTION_ERROR

    def clear_all_owners(self) -> None:
        """Tries to remove all owners from occupied gadgets."""
        status, gadgets = self.get_gadgets()
        if status == self.SUCCESS:
            for gadget in gadgets:
                self.drop_gadget_owner(gadget['id'])
